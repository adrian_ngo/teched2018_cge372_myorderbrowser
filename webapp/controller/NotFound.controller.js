sap.ui.define([
	"my/teched/app/d475/MyOrderBrowser/controller/BaseController"
	], function (BaseController) {
		"use strict";

		return BaseController.extend("my.teched.app.d475.MyOrderBrowser.controller.NotFound", {

			onInit: function () {
				this.getRouter().getTarget("notFound").attachDisplay(this._onNotFoundDisplayed, this);
			},

			_onNotFoundDisplayed : function () {
					this.getModel("appView").setProperty("/layout", "OneColumn");
			}
		});
	}
);